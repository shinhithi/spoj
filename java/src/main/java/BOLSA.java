public class BOLSA {

    private static int[] saida = new int[1000];

    public static void main(String[] args) throws java.io.IOException {

        String b;

        java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.InputStreamReader(System.in), 4096);
        int cont = 0;
        while (!(b = bf.readLine()).equals("0") ){
            int x = Integer.parseInt(b);
            int[] C = new int[x+1];
            int[] V= new int[x+1];
            int v = 0;
            int c = 0;
            int d = 0;

            for(int i = 1; i <= x; i++){

                b = bf.readLine();

                char t = b.charAt(0);

                int valor = Integer.valueOf(b.substring(2).replace(".", ""));

                if( t == 'C'){
                    C[++c] = valor;
                    if(c > 1) constroi_max_heap(c, C);
                }else{
                    V[++v] = valor;
                    if(v > 1 ) constroi_min_heap(v,V);
                }

                if( (c > 0 && v > 0) && V[1] <= C[1] ){
                    d += C[1] - V[1];
                    C[1] = 0;
                    constroi_max_heap(c, C);
                    c--;
					V[1] = 5000001;
                    constroi_min_heap(v,V);
                    v--;
                }
            }

            saida[cont++] = d;
        }

        for(int i = 0 ; i < cont; i++){
            System.out.format(java.util.Locale.ENGLISH,"%.2f%n", saida[i]/100.00);
        }
    }

    private static void constroi_max_heap( int n, int v[])
    {
        int m, f, t;
        for (m = 1; m < n; ++m) {
            f = m+1;
            while (f > 1 && v[f/2] < v[f]) {
                t = v[f/2]; v[f/2] = v[f]; v[f] = t;
                f = f/2;
            }
        }
    }

    private static void constroi_min_heap( int n, int v[])
    {
        int m, f, t;
        for (m = 1; m < n; ++m) {
            f = m+1;
            while (f > 1 && v[f/2] > v[f]) {
                t = v[f/2]; v[f/2] = v[f]; v[f] = t;
                f = f/2;
            }
        }
    }

}