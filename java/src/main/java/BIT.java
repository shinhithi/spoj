public class BIT{

    public static void main(String[] args) throws java.io.IOException {

        String v;

        java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder ();
        int cont = 1;
        while (!(v = bf.readLine()).equals("0") ){

            int x = Integer.parseInt(v);

            sb.append ("Teste ");
            sb.append( cont++ );
            sb.append("\n");

            sb.append( x / 50);
            sb.append(" ");
            x = x % 50;

            sb.append( x / 10);
            sb.append(" ");
            x = x % 10;

            sb.append( x / 5 );
            sb.append(" ");

            sb.append(x % 5 );

            sb.append("\n\n");
        }

        System.out.print(sb.toString());
    }

}