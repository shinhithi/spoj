#include <stdio.h>
#include <stdlib.h>

using namespace std;

void descer_max(int* v, int p, int m)
{ 
   int f = 2*p;
   int x = v[p];
   while (f <= m) {
      if (f < m && v[f] < v[f+1])  ++f;
      if (x >= v[f]) break;
      v[p] = v[f];
      p = f, f = 2*p;
   }
   v[p] = x;
}

void descer_min(int* v,int p, int m)
{ 
   int f = 2*p;
   int x = v[p];
   while (f <= m) {
      if (f < m && v[f] > v[f+1])  ++f;
      if (x <= v[f]) break;
      v[p] = v[f];
      p = f, f = 2*p;
   }
   v[p] = x;
}

void subir_max(int* v, int f)
{ 
   int p = f/2;
   int x = v[f];
   while (p >= 1) {
      if (x <= v[p]) break;
      v[f] = v[p];
      f = p, p = p/2;
   }
   v[f] = x;
}

void subir_min(int* v, int f)
{ 
   int p = f/2;
   int x = v[f];
   while (p >= 1) {
      if (x >= v[p]) break;
      v[f] = v[p];
      f = p, p = p/2;
   }
   v[f] = x;
}

int main() {
    int x, i, p, cont = 0;
    char ch;
    int f, f1;
    int saida[50000];
    int C[50001];
    int V[50001];
    int c;
    int v;
    int d;
    
    scanf("%i", &x);
    getchar();    
    while (x != 0) {

        p = d = v = c = 0;
        V[1] = 0;
        C[1] = 0;

        for (i = 1; i <= x; i++) {

            scanf("%c %d.%d", &ch, &f, &f1);
            getchar();

            if (ch == 'C') {
                C[++c] = f * 100 + f1;
                if(c > 1 ) subir_max(C, c);
            }else{
                V[++v] = f * 100 + f1;
                if(v > 1 ) subir_min(V, v);
            }
            
            if( v == 0 || c == 0 ) continue;
                        
            if( V[1] <= C[1]){
                d += (C[1] - V[1]);
                V[1] = V[v--];
                if(v > 1 ) descer_min(V, 1, v);
                C[1] = C[c--];
                if(c > 1) descer_max(C, 1, c);
            }
            
        }

        saida[cont++] = d;

        scanf("%i", &x);
	getchar();
    }

    for (i = 0; i < cont; i++) {
        f = saida[i] / 100;
        f1 = saida[i] % 100;
        printf("%d.%02d\n", f, f1 );
    }

    return 0;
}

