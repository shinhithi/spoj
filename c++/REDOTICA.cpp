#include <stdio.h>
#include <stdlib.h>

using namespace std;

struct Node
{
    int orig;
    int dest;
    int peso;
};

void descer_min(struct Node* v, int p, int m)
{ 
   int f = 2*p;
   struct Node x = v[p];
   while (f <= m) {
      if (f < m && v[f].peso > v[f+1].peso)  ++f;
      if (x.peso <= v[f].peso) break;
      v[p] = v[f];
      p = f, f = 2*p;
   }
   v[p] = x;
}

void subir_min(struct Node* v, int f)
{ 
   int p = f/2;
   struct Node x = v[f];
   while (p >= 1) {
      if (x.peso >= v[p].peso) break;
      v[f] = v[p];
      f = p, p = p/2;
   }
   v[f] = x;
}

void adiciona(struct Node* heap, int &n, int orig, int dest, int peso){
    n++;
    heap[n].orig = orig;
    heap[n].dest = dest;
    heap[n].peso = peso;
    subir_min(heap, n);
}

struct Node remove(struct Node* heap, int &n){
    struct Node x = heap[1];
    heap[1] = heap[n--];
    descer_min(heap, 1, n);
    return x;
}

int main() {
    int i, j, x, y, z, n, m, cont = 0, n_teste = 1;
    char saida[10101][10];
    int g[101][101];
    struct Node heap[4951];
    bool v[101];

    scanf("%i %i", &n, &m);
    getchar();    
    while (n != 0) {
        
        v[n] = false;
        for (i = 1; i < n; i++) {
            for(j= i + 1; j <= n; j++){
                g[i][j] = g[j][i] = 0;
            }
            v[i] = false;
        }        
        
        for (i = 1; i <= m; i++) {
            
            scanf("%i %i %i", &x, &y, &z);
            getchar();
            g[x][y] = g[y][x] = z;
                        
        }
        
        v[1] = true;
        x = 1;
        y = 0;
        sprintf (saida[cont++], "\nTeste %i", n_teste++);
        for(i = 1; i < n; i++){
            
            for(j = 1; j <= n; j++ ){
                
                if( !v[j] && g[x][j] > 0 ){
                    adiciona(heap, y, x, j, g[x][j]);
                }
                
            }
            
            struct Node aresta = remove(heap, y);
            while(v[aresta.dest] && v[aresta.orig]){
                aresta = remove(heap, y);
            }
            
            if(aresta.orig <  aresta.dest){
                sprintf (saida[cont++], "%i %i", aresta.orig, aresta.dest);
            }else{
                sprintf (saida[cont++], "%i %i", aresta.dest, aresta.orig);
            }
            
            x = v[aresta.dest] ? aresta.orig : aresta.dest;
            v[x] = true;

        }

        scanf("%i %i", &n, &m);
	getchar();
    }
    
    for (i = 0; i < cont; i++) {
        printf("%s\n", saida[i] );
    }

    return 0;
}

