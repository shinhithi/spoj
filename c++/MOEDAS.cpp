#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main() {
    int m, n, v, i, j, x, y, cont=0;
    char saida[50000][12];
    int OLD[50001];
    int MAT[50001];
    int *tmp, *O, *M;
    int V[101];
    

   while (scanf("%i", &m), m != 0) {
        scanf("%i", &n);

        for (i = 0; i != n; ++i) {
            scanf("%i", &v);
            V[i] = v;
        }
        
        O = OLD;
        M = MAT;
        
        for (i = 1; i <= m; ++i){
            O[i] = 50001;
        }
        O[0] = 0;
        
        for (j = 1; j <= n; ++j) {
            
            M[0] = 0;
            for (i = 1; i <= m; ++i) {
                v = V[j-1];
                x = O[i];
                y = 50001;
                if (v <= i)
                    y = 1 + M[i-v];
                M[i] = x < y ? x : y;
            }
         
            tmp = O;
            O = M;
            M = tmp;            
            
        }
        
        x = O[m];
        if (x >= 50001)
            sprintf (saida[++cont], "Impossivel");
        else
            sprintf (saida[++cont], "%i", x);
    }

    for (i = 0; i <= cont; ++i) {
        printf("%s\n", saida[i] );
    }
    
    return 0;
}

